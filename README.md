## 项目名称
> 慧链logger组件。 
基于logrus和file-rotatelogs打造 



## 运行条件
> 列出运行该项目所必须的条件和相关依赖  
* 在项目根目录下的添加 logger.ymal 用于配置logger


## 运行说明
> 说明如何运行和使用你的项目，建议给出具体的步骤说明
* 操作一
* 操作二
* 操作三  



## 测试说明
```cgo
logger：
    <logger组>：
        name: <logger name>,
        level: <lelve, debug to error>
        append: <console or file, 默认file>
        path: <日志目录，默认logs>
        
```

例如
```
logger:
  test1:
    name: test1
    level: info
    path: logs
  test2:
    name: test2
    level: debug
    path: logs
  test3:
    level: warn
    path: logs
  test4:
    level: debug
    append: console
```  



## 技术架构
* 就是使用logurs  


## 作者
* laoniu
