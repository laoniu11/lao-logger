package logger

import (
	fr "github.com/lestrrat-go/file-rotatelogs"
	"io"
	"time"
)

func frWrite(file string) (io.Writer, error) {
	w, err := fr.New(
		file + ".%Y-%m-%d",
		fr.WithLinkName(file),
		fr.WithMaxAge(time.Duration(24 * 7) * time.Hour),
		fr.WithRotationTime(time.Duration(24) * time.Hour))

	if err != nil {
		return nil, err
	}

	return w, nil
}
