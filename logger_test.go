package logger_test

import (
	logger "gitee.com/laoniu11/lao-logger"
	"testing"
)

func init()  {
	//logger.LoadConf("logger-test.yaml")
}

func TestLoadCongLog(t *testing.T) {
	logger.SetConfigFile("logger-test.yaml")

	doCheLog("test1", t)
	doCheLog("test2", t)
	doCheLog("test3", t)
}

func doCheLog(name string, t *testing.T) {
	log := logger.GetLog(name)
	if log == nil {
		t.Errorf("no such log[%v]", name)
		t.Fail()
		return
	}

	log.Warnln("hello " + name)
}

func TestLoadOnlyOnce(t *testing.T) {
	go func() {
		logger.LoadConf("logger-test.yaml")
	}()

	go func() {
		logger.LoadConf("logger-test.yaml")
	}()

	logger.LoadConf("logger-test.yaml")
}

func TestConsoleLog(t *testing.T) {
	doCheLog("test4", t)
}
